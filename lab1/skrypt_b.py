import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random as rnd

from numpy import linalg as LA

delta = 0.1
x_size = 10.0
y_size = 10.0
obst_vect = np.array([[rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)]])
start_point = np.array([-10, rnd.uniform(-10.0,10.0)])
finish_point = np.array([10, rnd.uniform(-10.0,10.0)])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = []

fly_distance = LA.norm(start_point-finish_point)

kp = 10
koi = 10*kp
d0 = 5

for y_i in y:
    row = np.array([])
    for x_i in x:
        q = np.array([x_i, y_i])
        Fp = kp * LA.norm(q-finish_point)
        sumFoi = 0.0
        for obst in obst_vect:
            if LA.norm(q - obst) == 0:
                sumFoi += 1000.0
            elif LA.norm(q - obst) <= d0:
                Foi = (koi * (1/(LA.norm(q - obst))-(1/d0)) * 1/((LA.norm(q - obst))**2)) #positive value

                sumFoi += Foi
            else:
                sumFoi += 0.0
        
        F = Fp + sumFoi
        if F > kp*(fly_distance+10): F =kp*(fly_distance+10)
        row=np.append(row, F)
    Z.append(row)

fig = plt.figure(figsize=(10.0, 10.0))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
