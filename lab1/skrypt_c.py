import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random as rnd

from numpy import linalg as LA

delta = 0.1
x_size = 10.0
y_size = 10.0
obst_vect = np.array([[rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)], 
                      [rnd.uniform(-10.0,10.0), rnd.uniform(-10.0,10.0)]])
start_point = np.array([-10, rnd.uniform(-10.0,10.0)])
finish_point = np.array([10, rnd.uniform(-10.0,10.0)])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = []

fly_distance = LA.norm(start_point-finish_point)

kp = 10
koi = 10*kp
d0 = 5

for y_i in y:
    row = np.array([])
    for x_i in x:
        q = np.array([x_i, y_i])
        Fp = kp * LA.norm(q-finish_point)
        sumFoi = 0.0
        for obst in obst_vect:
            if LA.norm(q - obst) == 0:
                sumFoi += 1000.0
            elif LA.norm(q - obst) <= d0:
                Foi = (koi * (1/(LA.norm(q - obst))-(1/d0)) * 1/((LA.norm(q - obst))**2)) #positive value

                sumFoi += Foi
            else:
                sumFoi += 0.0
        
        F = Fp + sumFoi
        if F > kp*(fly_distance+10): F =kp*(fly_distance+10)
        row=np.append(row, F)
    Z.append(row)

path = []
path_point = start_point
path.append(path_point)
path_point_x = 100+int( path_point[0]/delta)
path_point_y = 100+int( path_point[1]/delta)

print(path_point)
print(path_point_x)
print(path_point_y)
print(Z[path_point_x][path_point_y])
current_force = Z[path_point_x][path_point_y]
#we forze
while(current_force>2):
    path_point_x = 100+int( path_point[0]/delta)
    path_point_y = 100+int( path_point[1]/delta)
    #dsds
    current_force = Z[path_point_x][path_point_y]
    sides = []

    go_up = [path_point_x, path_point_y+1] 
    go_down = [path_point_x, path_point_y-1]
    go_left = [path_point_x-1, path_point_y]
    go_right = [path_point_x+1, path_point_y]

    go_up_left = [path_point_x-1, path_point_y+1]
    go_up_right = [path_point_x+1, path_point_y+1]
    go_down_left = [path_point_x-1, path_point_y-1]
    go_down_right = [path_point_x+1, path_point_y-1]

    sides.append(go_up)
    sides.append(go_down)
    sides.append(go_left)
    sides.append(go_right)
    sides.append(go_up_left)
    sides.append(go_up_right)
    sides.append(go_down_left)
    sides.append(go_down_right)
    next_step = path_point
    
    for side in sides:
        curr_x = side[0]
        curr_y = side[1]
        if(curr_x>=0 and curr_x<200 and curr_y>=0 and curr_y<200):
            if(Z[curr_x][curr_y]<=current_force):
                if(Z[curr_x][curr_y]<current_force):
                    next_step = [(curr_x-100)*delta, (curr_y-100)*delta]
                    current_force = Z[curr_x][curr_y]
                elif(Z[curr_x][curr_y]==current_force):
                    next_step = [(curr_x-100)*delta, (curr_y-100)*delta]
                    current_force = Z[curr_x][curr_y]
    
    if(path_point[0]==next_step[0] and path_point[1]==next_step[1]):
        print('break')
        break
        i=0
        while(1):
            side=sides[i]
            curr_x = side[0]
            curr_y = side[1]
            i=i+1
            if(curr_x>=0 and curr_x<200 and curr_y>=0 and curr_y<200):
                next_step = [(curr_x-100)*delta, (curr_y-100)*delta]
                current_force = Z[curr_x][curr_y]
                break

    path.append(next_step)
    path_point_x = 100+int(next_step[0]/delta)
    path_point_y = 100+int(next_step[1]/delta)
    print (path_point)
    print(LA.norm(path_point-finish_point))
    path_point = next_step

print(current_force)

fig = plt.figure(figsize=(10.0, 10.0))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for pth in path:
    plt.plot(pth[0], pth[1], "or", color='black')


plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
